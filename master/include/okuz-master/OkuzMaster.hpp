#ifndef OKUZMASTER_HPP
#define OKUZMASTER_HPP

#include <stdint.h>
#include <unistd.h>
#include <iostream>
#include <string>
#include <zmq.hpp>

namespace okuz { namespace master {

using namespace std;

class OkuzMaster
{
public:
	OkuzMaster(char* portName);
	~OkuzMaster();
	
	void oneWay(const uint8_t* pl, uint8_t plSize, uint8_t adr);
	void twoWay(const uint8_t* pl, uint8_t plSize,
		uint8_t* target, uint8_t rSize, uint8_t adr);
	void setTimeout(int ms) { timeout = ms; }
	
private:
	int pid;
	int timeout;
	zmq::context_t *context;
	zmq::socket_t *socket;

};

}} // End of namespace okuz::master

#endif // OKUZMASTER_HPP
