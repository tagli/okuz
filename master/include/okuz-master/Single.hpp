#ifndef STATIC_SINGLE_HPP
#define STATIC_SINGLE_HPP

#include "RemoteVar.hpp"

namespace okuz { namespace master {

template<typename T>
class Single : public RemoteVar
{
public:
	Single(OkuzMaster& om, uint16_t rd, uint16_t wr,
			uint16_t sv, uint16_t ld, uint8_t adr, bool ip = false);
	
	virtual T operator=(T input);
	virtual operator T();
	
	virtual void transmit();
	virtual void receive();
	
	virtual int getRawSize() { return sizeof(T); }
	virtual void rawDump(uint8_t* target);
	virtual void rawStore(uint8_t* source);
	
	// CLI related functions
	virtual void print();
	virtual void print(int memberIndex);
	virtual void setTransmit(vector<string> &param);
	virtual void setTransmit(int memberIndex, vector<string> &param);
	
protected:
	T data;
	
private:
	const bool inPack;
};

template<> void Single<int8_t>::print();
template<> void Single<uint8_t>::print();
template<> void Single<int8_t>::setTransmit(vector<string> &param);
template<> void Single<uint8_t>::setTransmit(vector<string> &param);

// ***** Single Member Functions *****

template<typename T>
Single<T>::Single(OkuzMaster& om, uint16_t rd, uint16_t wr,
		uint16_t sv, uint16_t ld, uint8_t adr, bool ip)
: RemoteVar(om, rd, wr, sv, ld, adr), inPack(ip), data(0)
{
}

template<typename T>
void Single<T>::transmit()
{
	const int payloadSize = sizeof(T) + sizeof(uint16_t); // data + accessCode
	uint8_t payload[payloadSize];
	*((uint16_t*)payload) = writeCode; // Placing the code in the beginning
	*((T*)(payload + sizeof(writeCode))) = data; // Placing the data
	channel.oneWay(payload, payloadSize, address);
}

template<typename T>
void Single<T>::receive()
{
	const int payloadSize = sizeof(uint16_t);
	const int responseSize = sizeof(T) + sizeof(uint16_t); // with msgType
	uint8_t payload[payloadSize];
	*((uint16_t*)payload) = readCode; // Placing the code in the beginning
	channel.twoWay(payload, payloadSize, (uint8_t*)&data, responseSize, address);
}

template<typename T>
T Single<T>::operator=(T input)
{
	data = input;
	if (!inPack && writeCode != 0) transmit();
	return input;
}

template<typename T>
Single<T>::operator T()
{
	if (!inPack && readCode != 0) receive();
	return data;
}

template<typename T>
void Single<T>::rawDump(uint8_t* target)
{
	int rawSize = this->getRawSize();
	uint8_t *ptr = (uint8_t*)(&data);
	for (int i = 0; i < rawSize; ++i) {
		target[i] = ptr[i];
	}
}

template<typename T>
void Single<T>::rawStore(uint8_t* source)
{
	int rawSize = this->getRawSize();
	uint8_t *ptr = (uint8_t*)(&data);
	for (int i = 0; i < rawSize; ++i) {
		ptr[i] = source[i];
	}
}

// This functions are implemented for okuz-cli usage
template<typename T>
void Single<T>::print()
{
	cout << data << endl;
}

template<typename T>
void Single<T>::print(int memberIndex)
{
	cout << "Member access is not available for Single type!" << endl;
}

template<typename T>
void Single<T>::setTransmit(vector<string> &param)
{
	std::stringstream ss;
	ss << param[0];
	ss >> data;
	if (!inPack && writeCode != 0) transmit();
}

template<typename T>
void Single<T>::setTransmit(int memberIndex, vector<string> &param)
{
	cout << "Submember assignments are not supported for Single!" << endl;
}

}} // End of namespace okuz::master

#endif // STATIC_SINGLE_HPP
