#ifndef STATIC_BITS_HPP
#define STATIC_BITS_HPP

#include <climits>
#include "Single.hpp"

namespace okuz { namespace master {

// Forward decleration for Bits
template<typename T>
class Bits;

template<typename T>
class _bitsHelper
{
public:
	_bitsHelper(Bits<T>* bm, int i);
	
	int operator=(int input);
	operator int();
	
private:
	Bits<T> *bitsMaster;
	int index;
};

template<typename T>
class Bits : public Single<T>
{
public:
	Bits(OkuzMaster& om, uint16_t rd, 
		uint16_t wr, uint16_t sv, uint16_t ld, uint8_t adr);
	void setBit(int index, unsigned int bit);
	int getBit(int index);
};

// ***** _bitsHelper Member Functions *****

template<typename T>
_bitsHelper<T>::_bitsHelper(Bits<T> *bm, int i)
: bitsMaster(bm), index(i)
{
}

template<typename T>
int _bitsHelper<T>::operator=(int input)
{
	if (input == 0)
		bitsMaster->setBit(index, 0);
	else
		bitsMaster->setBit(index, 1);
	return input;
}

template<typename T>
_bitsHelper<T>::operator int()
{
	return bitsMaster->getBit(index);
}

// ***** Bits Member Functions *****
template<typename T>
Bits<T>::Bits(OkuzMaster& om, uint16_t rd, 
	uint16_t wr, uint16_t sv, uint16_t ld, uint8_t adr)
: Single<T>(om, rd, wr, sv, ld, adr)
{
	this->data = 0;
}

template<typename T>
void Bits<T>::setBit(int index, unsigned int bit)
{
	this->receive();
	unsigned int mask;
	if (bit == 0) {
		mask = ~(1 << index);
		this->data &= mask;
	}
	else {
		mask = (1 << index);
		this->data |= mask;
	} 
	this->transmit();
}

template<typename T>
int Bits<T>::getBit(int index)
{
	this->receive();
	return (this->data >> index) & 1;
}

}} // End of namespace okuz::master

#endif // STATIC_BITS_HPP
