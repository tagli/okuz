#ifndef STATIC_REMOTEVAR_HPP
#define STATIC_REMOTEVAR_HPP

#include <stdint.h>
#include <sstream>
#include <vector>
#include "OkuzMaster.hpp"

namespace okuz { namespace master {
	
class RemoteVar
{
public:
	RemoteVar(OkuzMaster& om, uint16_t rd, 
			uint16_t wr, uint16_t sv, uint16_t ld, uint8_t adr);
	virtual ~RemoteVar() {}
	
	virtual void transmit() = 0;
	virtual void receive() = 0;
	virtual void save();
	virtual void load();
	
	virtual int getRawSize() = 0;
	virtual void rawDump(uint8_t* target) = 0;
	virtual void rawStore(uint8_t* source) = 0;
	
	// CLI related functions
	virtual void print() = 0;
	virtual void print(int memberIndex) = 0;
	virtual void setTransmit(vector<string> &param) = 0;
	virtual void setTransmit(int memberIndex, vector<string> &param) = 0;
	
protected:
	OkuzMaster& channel;
	const uint8_t address;
	
	const uint16_t readCode;
	const uint16_t writeCode;
	const uint16_t saveCode;
	const uint16_t loadCode;
};

}} // End of namespace okuz::master

#endif // STATIC_REMOTE_HPP
