#ifndef STATIC_MULTI_HPP
#define STATIC_MULTI_HPP

#include "RemoteVar.hpp"

namespace okuz { namespace master {

// Forward decleration for Multi
template<typename T>
class Multi;

template<typename T>
class _multiHelper
{
public:
	_multiHelper(Multi<T>* mm);
	void setIndex(int i) { index = i; }
	
	T operator=(T input);
	operator T();
	
private:
	Multi<T> *multiMaster;
	int index;
};


template<typename T>
class Multi : public RemoteVar
{
public:
	Multi(int s, OkuzMaster& om, uint16_t rd, uint16_t wr,
			uint16_t sv, uint16_t ld, uint8_t adr, bool ip = false);
	virtual ~Multi();
	
	virtual void transmit();
	virtual void receive();
	void hold();
	
	virtual int getRawSize() { return size * sizeof(T); }
	virtual void rawDump(uint8_t* target);
	virtual void rawStore(uint8_t* source);
	
	// CLI related functions
	virtual void print();
	virtual void print(int memberIndex);
	virtual void setTransmit(vector<string> &param);
	virtual void setTransmit(int memberIndex, vector<string> &param);
	
	_multiHelper<T>& operator[](int i);
	
	void setMember(int index, T input);
	T getMember(int index);
	
private:
	int size;
	T *data;
	_multiHelper<T> slave;
	bool holdActive;
	int holdCounter;
	const bool inPack;
};


// ***** _multiHelper Member Functions *****

template<typename T>
_multiHelper<T>::_multiHelper(Multi<T> *mm)
: multiMaster(mm), index(-1)
{
}

template<typename T>
T _multiHelper<T>::operator=(T input)
{
	multiMaster->setMember(index, input);
	return input;
}

template<typename T>
_multiHelper<T>::operator T()
{
	return multiMaster->getMember(index);
}


// ***** Multi Member Functions *****

template<typename T>
Multi<T>::Multi(int s, OkuzMaster& om, uint16_t rd, uint16_t wr,
		uint16_t sv, uint16_t ld, uint8_t adr, bool ip)
: RemoteVar(om, rd, wr, sv, ld, adr), size(s), slave(this), 
holdActive(false), holdCounter(0), inPack(ip)
{
	data = new T[size];
}

template<typename T>
Multi<T>::~Multi()
{
	delete[] data;
}

template<typename T>
void Multi<T>::hold()
{
	holdActive = true;
	holdCounter = size;
}

template<typename T>
void Multi<T>::transmit()
{
	const int payloadSize = size * sizeof(T) + sizeof(uint16_t); // data + accessCode
	uint8_t payload[payloadSize];
	*((uint16_t*)payload) = writeCode; // Placing the code in the beginning
	T *ptr = (T*)(payload + sizeof(writeCode));
	for (int i = 0; i < size; ++i) {
		ptr[i] = data[i];
	}
	channel.oneWay(payload, payloadSize, address);
}

template<typename T>
void Multi<T>::receive()
{
	const int payloadSize = sizeof(uint16_t);
	const int responseSize = size * sizeof(T) + sizeof(uint16_t); // with msgType
	uint8_t payload[payloadSize];
	*((uint16_t*)payload) = readCode; // Placing the code in the beginning
	channel.twoWay(payload, payloadSize, (uint8_t*)data, responseSize, address);
}

template<typename T>
_multiHelper<T>& Multi<T>::operator[](int i)
{
	slave.setIndex(i);
	return slave;
}

template<typename T>
void Multi<T>::setMember(int index, T input)
{
	data[index] = input;
	if (!holdActive && !inPack) {
		transmit();
	}
	else { // Hold is active
		if (--holdCounter == 0 && !inPack) {
			holdActive = false;
			transmit();
		}
	}
}

template<typename T>
T Multi<T>::getMember(int index)
{
	if (!holdActive && !inPack) {
		receive();
	}
	else { // Hold is active
		if (holdCounter == size && !inPack) {
			receive();
		}
		if (--holdCounter == 0) {
			holdActive = false;
		}
	}
	return data[index];
}

template<typename T>
void Multi<T>::rawDump(uint8_t* target)
{
	int rawSize = this->getRawSize();
	uint8_t *ptr = (uint8_t*)data;
	for (int i = 0; i < rawSize; ++i) {
		target[i] = ptr[i];
	}
}

template<typename T>
void Multi<T>::rawStore(uint8_t* source)
{
	int rawSize = this->getRawSize();
	uint8_t *ptr = (uint8_t*)data;
	for (int i = 0; i < rawSize; ++i) {
		ptr[i] = source[i];
	}
}

// This functions are implemented for okuz-cli usage
template<typename T>
void Multi<T>::print()
{
	for (int i = 0; i < size; ++i) cout << data << " ";
	cout << endl;
}

template<typename T>
void Multi<T>::print(int memberIndex)
{
	cout << data[memberIndex] << endl;
}

template<typename T>
void Multi<T>::setTransmit(vector<string> &param)
{
	std::stringstream ss;
	for (int i = 0; i < param.size(); ++i) {
		ss << param[i];
		ss >> data[i];
	}
	if (!inPack && writeCode != 0) transmit();
}

template<typename T>
void Multi<T>::setTransmit(int memberIndex, vector<string> &param)
{
	cout << "Submember assignments are not supported for Multi!" << endl;
}

}} // End of namespace okuz::master

#endif // STATIC_MULTI_HPP
