#ifndef STATIC_PACK_HPP
#define STATIC_PACK_HPP

#include <vector>
#include "RemoteVar.hpp"
#include "Single.hpp"
#include "Multi.hpp"

class CliPair;

namespace okuz { namespace master {

class Pack : public RemoteVar
{
	friend class ::CliPair;
public:
	Pack(OkuzMaster& om, uint16_t rd, uint16_t wr,
			uint16_t sv, uint16_t ld, uint8_t adr, int nM);
	virtual ~Pack();

	virtual void transmit();
	virtual void receive();

	virtual int getRawSize();
	virtual void rawDump(uint8_t* target);
	virtual void rawStore(uint8_t* source);
	
	// CLI related functions
	virtual void print();
	virtual void print(int memberIndex);
	virtual void setTransmit(vector<string> &param);
	virtual void setTransmit(int memberIndex, vector<string> &param);
	
protected:
	void allocateBuffer();

	const int nMember;
	std::vector<RemoteVar*> members;
	uint8_t *commBuf;
};

}} // End of namespace okuz::master

#endif // STATIC_PACK_HPP
