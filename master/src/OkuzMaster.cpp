#include "okuz-master/OkuzMaster.hpp"

namespace okuz { namespace master {

OkuzMaster::OkuzMaster(char* portName)
: timeout(100)
{
	pid = getpid();
	std::cout << "OkuzMaster PID: " << pid << std::endl;
	std::cout << "Port name: " << portName << std::endl;
	
	context = new zmq::context_t(1);
	socket = new zmq::socket_t(*context, ZMQ_REQ);
	socket->connect("ipc:///tmp/mdport");
}

OkuzMaster::~OkuzMaster()
{
	delete socket;
	delete context;
}

void OkuzMaster::oneWay(const uint8_t* pl, uint8_t plSize, uint8_t adr)
{
	// Sending the request

	zmq::message_t request (15 + plSize + 1); // +1 for plSize on TX line
    uint8_t buffer[15 + plSize + 1];

	*((int*)(buffer)) = 1; // Version
	*((int*)(buffer + 4)) = pid;
	*((int*)(buffer + 8)) = 0; // timeout
	*((uint8_t*)(buffer + 12)) = 0; // response size
	*((uint8_t*)(buffer + 13)) = adr; // address
	*((uint8_t*)(buffer + 14)) = plSize + 1; // payload size
	*((uint8_t*)(buffer + 15)) = plSize; // This one will be on TX line
	for (int i = 0; i < plSize; ++i)
		*((uint8_t*)(buffer + 16 + i)) = pl[i];

	memcpy((void *) request.data(), buffer, 15 + plSize + 1);
	socket->send(request);

	//  Getting the response.
	zmq::message_t response;
	socket->recv(&response);
	// TODO: Process response?
}

void OkuzMaster::twoWay(const uint8_t* pl, uint8_t plSize,
		uint8_t* target, uint8_t rSize, uint8_t adr)
{
	// Sending the request

	zmq::message_t request (15 + plSize + 1); // +1 for plSize on TX
    uint8_t buffer[15 + plSize + 1];

	*((int*)(buffer)) = 1; // Version
	*((int*)(buffer + 4)) = pid;
	*((int*)(buffer + 8)) = timeout;
	*((uint8_t*)(buffer + 12)) = rSize + 1; // RX also includes size byte
	*((uint8_t*)(buffer + 13)) = adr; // address
	*((uint8_t*)(buffer + 14)) = plSize + 1; // payload size
	*((uint8_t*)(buffer + 15)) = plSize; // This one will be on TX line
	for (int i = 0; i < plSize; ++i)
		*((uint8_t*)(buffer + 16 + i)) = pl[i];

	memcpy((void *) request.data(), buffer, 15 + plSize + 1);
	socket->send(request);

	//  Getting the response.
	zmq::message_t response;
	socket->recv(&response);
	// TODO: Check for errors or timeout?
	uint8_t *respStart = (uint8_t*)response.data();
	memcpy(target, respStart + 10 + 3, rSize); // +3 skip size & msgType
}

}} // End of namespace okuz::master
