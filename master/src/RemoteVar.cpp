#include "okuz-master/RemoteVar.hpp"

namespace okuz { namespace master {

// ***** Remote Member Functions *****

RemoteVar::RemoteVar(OkuzMaster& om, uint16_t rd, 
	uint16_t wr, uint16_t sv, uint16_t ld, uint8_t adr)
: channel(om), readCode(rd), writeCode(wr), saveCode(sv), loadCode(ld),
address(adr)
{
}

void RemoteVar::save()
{
	const int payloadSize = sizeof(uint16_t); // accessCode
	uint8_t payload[payloadSize];
	*((uint16_t*)payload) = saveCode; // Placing the accessCode
	channel.oneWay(payload, payloadSize, address);
}

void RemoteVar::load()
{
	const int payloadSize = sizeof(uint16_t); // accessCode
	uint8_t payload[payloadSize];
	*((uint16_t*)payload) = loadCode; // Placing the accessCode
	channel.oneWay(payload, payloadSize, address);
	receive(); // Update the local data
}

}} // End of namespace okuz::master
