#include "okuz-master/Single.hpp"

namespace okuz { namespace master {

template<>
void Single<int8_t>::print()
{
	cout << (int16_t)data << endl;
}

template<>
void Single<uint8_t>::print()
{
	cout << (uint16_t)data << endl;
}

template<>
void Single<int8_t>::setTransmit(vector<string> &param)
{
	int16_t temp;
	std::stringstream ss;
	ss << param[0];
	ss >> temp;
	data = (int8_t)temp;
	if (!inPack && writeCode != 0) transmit();
}

template<>
void Single<uint8_t>::setTransmit(vector<string> &param)
{
	uint16_t temp;
	std::stringstream ss;
	ss << param[0];
	ss >> temp;
	data = (uint8_t)temp;
	if (!inPack && writeCode != 0) transmit();
}

}} // End of namespace okuz::master
