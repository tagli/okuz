#include "okuz-master/Pack.hpp"
#include <climits>

namespace okuz { namespace master {

Pack::Pack(OkuzMaster& om, uint16_t rd, uint16_t wr,
		uint16_t sv, uint16_t ld, uint8_t adr, int nM)
: RemoteVar(om, rd, wr, sv, ld, adr), nMember(nM)
{
}

Pack::~Pack()
{
	delete[] commBuf;
}

void Pack::transmit()
{
	// Buffering raw dumps
	std::vector<RemoteVar*>::iterator itr;
	uint8_t *rawPtr = commBuf + 2; // +2 : Leave space for access code
	for (itr = members.begin(); itr != members.end(); ++itr) {
		int size = (*itr)->getRawSize();
		(*itr)->rawDump(rawPtr);
		rawPtr += size;
	}
	
	// Transmitting the buffer
	const int payloadSize = getRawSize() + sizeof(uint16_t); // data + accessCode
	*((uint16_t*)commBuf) = writeCode; // Placing the code in the beginning
	channel.oneWay(commBuf, payloadSize, address);
}

void Pack::receive()
{
	// Receiving into the buffer
	const int payloadSize = sizeof(uint16_t);
	const int responseSize = getRawSize() + sizeof(uint16_t); // with msgType
	uint8_t payload[payloadSize];
	*((uint16_t*)payload) = readCode; // Placing the code in the beginning
	channel.twoWay(payload, payloadSize, commBuf, responseSize, address);
	
	// Moving from buffer to members
	std::vector<RemoteVar*>::iterator itr;
	uint8_t *rawPtr = commBuf; // twoWay doesn't return msgId, just data
	for (itr = members.begin(); itr != members.end(); ++itr) {
		int size = (*itr)->getRawSize();
		(*itr)->rawStore(rawPtr);
		rawPtr += size;
	}
}

int Pack::getRawSize()
{
	std::vector<RemoteVar*>::iterator itr;
	int size = 0;
	for (itr = members.begin(); itr != members.end(); ++itr) {
		size += (*itr)->getRawSize();
	}
	return size;
}

void Pack::rawDump(uint8_t* target)
{
	cout << "rawDump is not implemented for Pack." << endl;
}

void Pack::rawStore(uint8_t* source)
{
	cout << "rawStore is not implemented for Pack." << endl;
}

void Pack::allocateBuffer()
{
	commBuf = new uint8_t[getRawSize() + sizeof(uint16_t)];
}

// This functions are implemented for okuz-cli usage
void Pack::print()
{
	vector<RemoteVar*>::iterator rv_it;
	for (rv_it = members.begin(); rv_it != members.end(); ++rv_it) {
		(*rv_it)->print();
	}
}

void Pack::print(int memberIndex)
{
	members[memberIndex]->print();
}

void Pack::setTransmit(vector<string> &param)
{
	cout << "Full assignments are not supported for Pack!" << endl;
}

void Pack::setTransmit(int memberIndex, vector<string> &param)
{
	members[memberIndex]->setTransmit(param);
	transmit();
}

}} // End of namespace okuz::master
