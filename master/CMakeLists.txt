cmake_minimum_required(VERSION 2.8)
project(master)

include_directories(${PROJECT_SOURCE_DIR}/include)
add_subdirectory(src)

install(
	DIRECTORY ${PROJECT_SOURCE_DIR}/include/okuz-master
	DESTINATION include
)

install(
	FILES ${PROJECT_SOURCE_DIR}/libokuzmaster.pc
	DESTINATION lib/pkgconfig
)
