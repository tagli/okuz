#include <iostream>
#include <unistd.h>
#include <string>
#include <vector>
#include <algorithm>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>

#include <stdio.h>
#include <readline/readline.h>
#include <readline/history.h>

#include "CliPair.hpp"
#include "okuz-parser/OkuzData.hpp"
#include "okuz-master/OkuzMaster.hpp"

using namespace std;

// Local Variables & Definitions
enum InputState {CMD, GET_WATCH, SET, SAVE, LOAD, INVALID};
vector<CliPair*> readablePairs;
vector<CliPair*> writablePairs;
vector<CliPair*> eepromablePairs;
char **readableWords, **writableWords, **eepromableWords;
int readableCount, writableCount, eepromableCount;

// Local Function Prototypes
CliPair* fetchCliPair(std::vector<CliPair*>& list, std::string name);
void tokenize(const string& str, vector<string>& tokens, const string& delimiters);
InputState getInputState(void);
char* inputGenerator(const char* text, int state);

int main(int argc, char* argv[])
{
	string okuzFile, channel;
	vector<uint8_t> adrList;
	okuz::parser::OkuzData od;
	int argIndex;
	int c;
	
	while ((c = getopt(argc, argv, "c:f:a:")) != -1) {
		switch (c) {
		case 'c':
			channel = string(optarg);
			cout << "Communication channel: " << channel << endl;
			break;
		case 'f':
			okuzFile = string(optarg);
			cout << "okuz File : " << okuzFile << endl;
			break;
		case 'a':
			adrList.push_back(atoi(optarg));
			cout << "First device address: " << (int)adrList[0] << endl;
			break;
		case '?':
			cout << "Something is wrong!\n";
			break;
		}
	}
	
	// Checking for required arguments
	if (okuzFile.empty()) {
		cout << "Error! Input file is missing.\n";
		exit(EXIT_FAILURE);
	}
	
	if (channel.empty()) {
		cout << "Using default channel: ttyUSB0\n";
		channel = string("ttyUSB0");
	}
	if (adrList.size() == 0) {
		cout << "Using default target address: 1\n";
		adrList.push_back(1);
	}
	
	// Generating CliPair objects
	od.parse(okuzFile);
	okuz::master::OkuzMaster om((char*)channel.c_str());
	vector<okuz::parser::RemoteVar*> parserNodes;
	parserNodes = od.getNodeList();
	vector<okuz::parser::RemoteVar*>::iterator p_it;
	vector<CliPair*> cliPairList;
	
	for (p_it = parserNodes.begin(); p_it != parserNodes.end(); ++p_it) {
		CliPair *tempPair = new CliPair(om, adrList[0], *p_it);
		cliPairList.push_back(tempPair);
	}
	
	// Sorting CliPair objects according to their flags
	vector<CliPair*>::iterator cli_it;
	for (cli_it = cliPairList.begin(); cli_it != cliPairList.end(); ++cli_it) {
		if ((*cli_it)->isReadable()) readablePairs.push_back(*cli_it);
		if ((*cli_it)->isWritable()) writablePairs.push_back(*cli_it);
		if ((*cli_it)->isEepromable()) eepromablePairs.push_back(*cli_it);
	}
	
	// Generating word lists for tab completion
	int wordIndex;
	
	readableCount = 0;
	for (cli_it = readablePairs.begin(); cli_it != readablePairs.end(); ++cli_it) {
		readableCount += (*cli_it)->getWordCount();
	}
	readableWords = (char**)malloc(readableCount * sizeof(char*));
	wordIndex = 0;
	for (cli_it = readablePairs.begin(); cli_it != readablePairs.end(); ++cli_it) {
		char **tempWordList = (*cli_it)->getWordList();
		int nWordsInPair = (*cli_it)->getWordCount();
		for (int i = 0; i < nWordsInPair; ++i) {
			readableWords[wordIndex++] = tempWordList[i];
		}
	}
	
	writableCount = 0;
	for (cli_it = writablePairs.begin(); cli_it != writablePairs.end(); ++cli_it) {
		writableCount += (*cli_it)->getWordCount();
	}
	writableWords = (char**)malloc(writableCount * sizeof(char*));
	wordIndex = 0;
	for (cli_it = writablePairs.begin(); cli_it != writablePairs.end(); ++cli_it) {
		char **tempWordList = (*cli_it)->getWordList();
		int nWordsInPair = (*cli_it)->getWordCount();
		for (int i = 0; i < nWordsInPair; ++i) {
			writableWords[wordIndex++] = tempWordList[i];
		}
	}
	
	eepromableCount = eepromablePairs.size();
	eepromableWords = (char**)malloc(eepromableCount * sizeof(char*));
	wordIndex = 0;
	for (cli_it = eepromablePairs.begin(); cli_it != eepromablePairs.end(); ++cli_it) {
		char **tempWordList = (*cli_it)->getWordList();
		eepromableWords[wordIndex++] = tempWordList[0]; // Sub-members are ignored
	}
	
	// Executing user commands
	rl_completion_entry_function = inputGenerator;
	char *userInput;
	while (1) {
		userInput = readline("okuz> ");
		if (userInput[0] != 0) add_history(userInput);
		string inputString = string(userInput);
		vector<string> inputTokens;
		vector<string> nameTokens;
		vector<string> parameters;
		tokenize(inputString, inputTokens, " ");
		tokenize(inputTokens[1], nameTokens, ".");
		
		// Command Parsing
		CliPair *fetchedPair;
		if (inputTokens[0] == "get") {
			fetchedPair = fetchCliPair(readablePairs, nameTokens[0]);
			if (nameTokens.size() == 1) fetchedPair->fetchPrint();
			else fetchedPair->fetchPrint(nameTokens[1]);
		}
		if (inputTokens[0] == "watch") {
			fetchedPair = fetchCliPair(readablePairs, nameTokens[0]);
			while (1) {
				if (nameTokens.size() == 1) fetchedPair->fetchPrint();
				else fetchedPair->fetchPrint(nameTokens[1]);
				usleep(500000);
				cout << "----------\n";
			}
		}
		else if (inputTokens[0] == "set") {
			fetchedPair = fetchCliPair(writablePairs, nameTokens[0]);
			vector<string> temp(inputTokens.begin() + 2, inputTokens.end());
			if (nameTokens.size() == 1) fetchedPair->setTransmit(temp);
			else fetchedPair->setTransmit(nameTokens[1], temp);
		}
		else if (inputTokens[0] == "save") {
			fetchedPair = fetchCliPair(eepromablePairs, nameTokens[0]);
			fetchedPair->save();
		}
		else if (inputTokens[0] == "load") {
			fetchedPair = fetchCliPair(eepromablePairs, nameTokens[0]);
			fetchedPair->load();
		}
		
		free(userInput);
	}
	
	
	return 0;
}

// Local Functions

char* inputGenerator(const char* text, int state) {
	static int listIndex, len;
	char *name;
	const char **possibleWords;
	static int nWords;
	const char *commands[] = {"get", "watch", "set", "save", "load"};
	InputState inState;
	
	if (state == 0) {
		listIndex = 0;
		len = strlen(text);
		inState = getInputState();
		switch (inState) {
		case CMD:
			possibleWords = commands;
			nWords = 5;
			break;
		case GET_WATCH:
			possibleWords = (const char**)readableWords;
			nWords = readableCount;
			break;
		case SET:
			possibleWords = (const char**)writableWords;
			nWords = writableCount;
			break;
		case SAVE:
		case LOAD:
			possibleWords = (const char**)eepromableWords;
			nWords = eepromableCount;
			break;
		case INVALID:
			possibleWords = NULL;
			nWords = 0;
			break;
		}
	}
	
	while (listIndex < nWords && possibleWords != NULL) {
		name = (char*)possibleWords[listIndex];
		++listIndex;
		if (strncmp(name, text, len) == 0) return strdup(name);
	}
	
	return NULL;
}

InputState getInputState(void)
{
	char *index = rl_line_buffer;
	int pos = 0;
	char cmdBuffer[10];
	
	while (*index == ' ') {
		if (pos >= rl_point) return CMD;
		++index;
		++pos;
	}
	char *start = index;
	
	int i = 0;
	while (*index != ' ') {
		if (pos >= rl_point) return CMD;
		cmdBuffer[i++] = *index;
		++index;
		++pos;
	}
	char *end = index;
	cmdBuffer[i] = '\0';
	
	if (strcmp(cmdBuffer, "get") == 0 || strcmp(cmdBuffer, "watch") == 0)
		return GET_WATCH;
	else if (strcmp(cmdBuffer, "set") == 0) return SET;
	else if (strcmp(cmdBuffer, "save") == 0) return SAVE;
	else if (strcmp(cmdBuffer, "load") == 0) return LOAD;
	else return INVALID;
}

CliPair* fetchCliPair(std::vector<CliPair*>& list, std::string name)
{
	vector<CliPair*>::iterator cp_it;
	for (cp_it = list.begin(); cp_it != list.end(); ++cp_it) {
		if ((*cp_it)->getBaseName() == name) return *cp_it;
	}
	cout << "No CliPair named " << name << endl;
	return 0;
}

// http://www.sbin.org/doc/HOWTO/C++Programming-HOWTO-7.html
void tokenize(const string& str, vector<string>& tokens, const string& delimiters)
{
    // Skip delimiters at beginning.
    string::size_type lastPos = str.find_first_not_of(delimiters, 0);
    // Find first "non-delimiter".
    string::size_type pos = str.find_first_of(delimiters, lastPos);

    while (string::npos != pos || string::npos != lastPos)
    {
        // Found a token, add it to the vector.
        tokens.push_back(str.substr(lastPos, pos - lastPos));
        // Skip delimiters.  Note the "not_of"
        lastPos = str.find_first_not_of(delimiters, pos);
        // Find next "non-delimiter"
        pos = str.find_first_of(delimiters, lastPos);
    }
}
