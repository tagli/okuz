#ifndef CLIPAIR_HPP
#define CLIPAIR_HPP

#include <iostream>
#include <string>
#include <cstdio>

#include <okuz-parser/RemoteVar.hpp>
#include <okuz-parser/Basic.hpp>
#include <okuz-parser/Bits.hpp>
#include <okuz-parser/Pack.hpp>

#include <okuz-master/OkuzMaster.hpp>
#include <okuz-master/RemoteVar.hpp>
#include <okuz-master/Single.hpp>
#include <okuz-master/Multi.hpp>
#include <okuz-master/Bits.hpp>
#include <okuz-master/Pack.hpp>

using okuz::master::OkuzMaster;

class CliPair
{
public:
	CliPair(OkuzMaster& om, int adr, okuz::parser::RemoteVar* s);
	~CliPair();
	
	// Getters
	std::string getBaseName() { return source->getName(); }
	okuz::parser::RemoteType getRemoteType() { return source->getRemoteType(); }
	char** getWordList() {return wordList; }
	int getWordCount() { return wordCount; }
	bool isReadable() { return source->getReadCode() != 0; }
	bool isWritable() { return source->getWriteCode() != 0; }
	bool isEepromable() { return source->getSaveCode() != 0; }
	
	// CLI related functions
	void fetchPrint();
	void fetchPrint(std::string member);
	void setTransmit(std::vector<std::string> &param);
	void setTransmit(std::string &sub, std::vector<std::string> &param);
	bool isSubmember(std::string &sub);
	void save();
	void load();
	
private:
	okuz::master::RemoteVar* generateBasic(OkuzMaster& om, int adr, 
		okuz::parser::RemoteVar* s, bool inPack = false);
	
	int wordCount;
	char** wordList;
	okuz::parser::RemoteVar* source;
	okuz::master::RemoteVar* avatar;
};

#endif
