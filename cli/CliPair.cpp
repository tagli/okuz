#include "CliPair.hpp"

CliPair::CliPair(OkuzMaster& om, int adr, okuz::parser::RemoteVar* s)
: source(s)
{
	uint16_t rd = s->getReadCode();
	uint16_t wr = s->getWriteCode();
	uint16_t sv = s->getSaveCode();
	uint16_t ld = s->getLoadCode();
	
	int size; // Dummy variable
	std::vector<okuz::parser::Basic*> memberList;
	std::vector<okuz::parser::Basic*>::iterator it;
	
	switch (s->getRemoteType()) {
	case okuz::parser::BASIC:
		avatar = generateBasic(om, adr, s);
		break;
	case okuz::parser::BITS:
		size = ((okuz::parser::Bits*)(s))->getBitList().size();
		if (size <= 8) {
			avatar = new okuz::master::Bits<uint8_t>(om, rd, wr, sv, ld, adr);
		}
		else {
			avatar = new okuz::master::Bits<uint8_t>(om, rd, wr, sv, ld, adr);
		}
		break;
	case okuz::parser::PACK:
		memberList = ((okuz::parser::Pack*)(s))->getMemberList();
		avatar = new okuz::master::Pack(om, rd, wr, sv, ld, adr, memberList.size());
		okuz::master::Pack* a2 = (okuz::master::Pack*)(avatar);
		a2->members.reserve(memberList.size());
		for (it = memberList.begin(); it != memberList.end(); ++it) {
			okuz::master::RemoteVar* tempRemote = generateBasic(om, adr, *it, true);
			a2->members.push_back(tempRemote);
		}
		a2->allocateBuffer();
		break;
	}
	
	// Generating word list for GNU Readline
	using namespace std;
	vector<string> memberNames = source->getMemberNames();
	wordCount = memberNames.size() + 1;  // Including name of itself
	wordList = (char**)malloc(wordCount * sizeof(char*));
	string baseName = source->getName();
	wordList[0] = (char*)malloc((baseName.size() + 1) * sizeof(char));
	sprintf(wordList[0], "%s", baseName.c_str());
	for (int i = 0; i < memberNames.size(); ++i) {
		wordList[i + 1] = (char*)malloc((baseName.size() + memberNames[i].size() + 2) * sizeof(char));
		sprintf(wordList[i + 1], "%s.%s", baseName.c_str(), memberNames[i].c_str());
	}
}

okuz::master::RemoteVar* CliPair::generateBasic(OkuzMaster& om, int adr,
	okuz::parser::RemoteVar* s, bool inPack)
{
	okuz::master::RemoteVar* avatar;
	int size = ((okuz::parser::Basic*)(s))->getSize();
	uint16_t rd = s->getReadCode();
	uint16_t wr = s->getWriteCode();
	uint16_t sv = s->getSaveCode();
	uint16_t ld = s->getLoadCode();
	if (size == 1) {
		switch (((okuz::parser::Basic*)(s))->getDataType()) {
		case okuz::parser::INT8:
			avatar = new okuz::master::Single<int8_t>(om, rd, wr, sv, ld, adr, inPack);
			break;
		case okuz::parser::INT16:
			avatar = new okuz::master::Single<int16_t>(om, rd, wr, sv, ld, adr, inPack);
			break;
		case okuz::parser::INT32:
			avatar = new okuz::master::Single<int32_t>(om, rd, wr, sv, ld, adr, inPack);
			break;
		case okuz::parser::UINT8:
			avatar = new okuz::master::Single<uint8_t>(om, rd, wr, sv, ld, adr, inPack);
			break;
		case okuz::parser::UINT16:
			avatar = new okuz::master::Single<uint16_t>(om, rd, wr, sv, ld, adr, inPack);
			break;
		case okuz::parser::UINT32:
			avatar = new okuz::master::Single<uint32_t>(om, rd, wr, sv, ld, adr, inPack);
			break;
		case okuz::parser::FLOAT32:
			avatar = new okuz::master::Single<float>(om, rd, wr, sv, ld, adr, inPack);
			break;
		}
	}
	else {
		switch (((okuz::parser::Basic*)(s))->getDataType()) {
		case okuz::parser::INT8:
			avatar = new okuz::master::Multi<int8_t>(size, om, rd, wr, sv, ld, adr, inPack);
			break;
		case okuz::parser::INT16:
			avatar = new okuz::master::Multi<int16_t>(size, om, rd, wr, sv, ld, adr, inPack);
			break;
		case okuz::parser::INT32:
			avatar = new okuz::master::Multi<int32_t>(size, om, rd, wr, sv, ld, adr, inPack);
			break;
		case okuz::parser::UINT8:
			avatar = new okuz::master::Multi<uint8_t>(size, om, rd, wr, sv, ld, adr, inPack);
			break;
		case okuz::parser::UINT16:
			avatar = new okuz::master::Multi<uint16_t>(size, om, rd, wr, sv, ld, adr, inPack);
			break;
		case okuz::parser::UINT32:
			avatar = new okuz::master::Multi<uint32_t>(size, om, rd, wr, sv, ld, adr, inPack);
			break;
		case okuz::parser::FLOAT32:
			avatar = new okuz::master::Multi<float>(size, om, rd, wr, sv, ld, adr, inPack);
			break;
		}
	}
	return avatar;
}

void CliPair::fetchPrint()
{
	using namespace std;
	vector<string> memberNames;
	int nMembers;
	switch (source->getRemoteType()) {
	case okuz::parser::BASIC:
	case okuz::parser::BITS:
		avatar->receive();
		avatar->print();
		break;
	case okuz::parser::PACK:
		memberNames = ((okuz::parser::Pack*)(source))->getMemberNames();
		nMembers = memberNames.size();
		avatar->receive();
		for (int i = 0; i < nMembers; ++i) {
			cout << memberNames[i] << ": ";
			avatar->print(i);
		}
		break;
	}
}

void CliPair::fetchPrint(std::string member)
{
	// To be implemented later.
}

void CliPair::setTransmit(std::vector<std::string> &param)
{
	avatar->setTransmit(param);
}

void CliPair::setTransmit(std::string &sub, std::vector<std::string> &param)
{
	using namespace std;
	int memberIndex = 0;
	vector<string> memberNames = source->getMemberNames();
	vector<string>::iterator s_it;
	while (memberIndex < memberNames.size()) {
		if (memberNames[memberIndex] == sub) break;
		memberIndex++;
	}
	avatar->setTransmit(memberIndex, param);
}

void CliPair::save()
{
	avatar->save();
}

void CliPair::load()
{
	avatar->load();
}

bool CliPair::isSubmember(std::string &sub) {
	using namespace std;
	vector<string> memberNames = source->getMemberNames();
	vector<string>::iterator s_it;
	for (s_it = memberNames.begin(); s_it != memberNames.end(); ++s_it) {
		if (*s_it == sub) return true;
	}
	return false;
}
