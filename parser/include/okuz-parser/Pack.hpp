#ifndef PARSER_PACK_HPP
#define PARSER_PACK_HPP

#include <vector>
#include <string>
#include "RemoteVar.hpp"
#include "Basic.hpp"

namespace okuz { namespace parser {

class Pack : public RemoteVar
{
public:
	Pack(std::string& n);
	virtual ~Pack();
	
	void addMember(Basic* m);
	
	// Getters
	const std::vector<Basic*>& getMemberList() const { return memberList; }
	const std::vector<std::string> getMemberNames();
	virtual std::string getDefinitionLine();
	virtual std::string getDefinitionLine2();
	virtual std::string getMemberInitLine();
	
private:
	std::vector<Basic*> memberList;
	std::vector<std::string> memberNames;
	bool memberNamesReady;
};

}} // End of namespace okuz::parser

#endif
