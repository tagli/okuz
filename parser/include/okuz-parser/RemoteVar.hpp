#ifndef PARSER_REMOTEVAR_HPP
#define PARSER_REMOTEVAR_HPP

#include <stdint.h>
#include <string>
#include <sstream>
#include <vector>

namespace okuz { namespace parser {
	
enum RemoteType {
	BASIC,
	PACK,
	BITS
};

class RemoteVar
{
public:
	
	RemoteVar(std::string& n);
	virtual ~RemoteVar();
	
	void addFlag(char f);
	int assignAccessCodes(int startId, int idInc);
	uint32_t assignEepromOffset(uint32_t adr);
	
	// Getters
	std::string getName() const { return name; }
	virtual const std::vector<std::string> getMemberNames() = 0;
	RemoteType getRemoteType() const { return rType; }
	int getRawSize() const { return rawSize; }
	virtual std::string getDefinitionLine() = 0;
	virtual std::string getDefinitionLine2() = 0;
	virtual std::string getMemberInitLine() = 0;
	virtual std::string getPointerString();
	bool isVolatile() { return volat; }
	int getReadCode() { return readCode; }
	int getWriteCode() { return writeCode; }
	int getSaveCode() { return saveCode; }
	int getLoadCode() { return loadCode; }
	uint32_t getEepromOffset() { return eepromOffset; }
	
protected:
	
	std::string name;
	RemoteType rType;
	int rawSize; // Raw size in bytes
	uint32_t dataAdr; // Register address in target
	uint32_t eepromOffset; // Eeprom address relative to start
	
	bool readable;
	bool writable;
	bool eepromable;
	bool volat; // Volatile
	
	int readCode;
	int writeCode;
	int saveCode;
	int loadCode;
	
};

}} // End of namespace okuz::parser

#endif
