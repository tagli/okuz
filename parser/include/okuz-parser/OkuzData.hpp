#ifndef OKUZDATA_HPP
#define OKUZDATA_HPP

#include <string>
#include <vector>
#include "OkuzParser.hpp"

#include "Basic.hpp"
#include "Bits.hpp"
#include "Pack.hpp"
#include "RemoteVar.hpp"

#define YY_DECL okuz::parser::OkuzParser::symbol_type yylex(okuz::parser::OkuzData& od)
YY_DECL;

namespace okuz { namespace parser {

class OkuzData
{
public:
	OkuzData();
	~OkuzData();
	
	int parse (const std::string& fileName);
	RemoteVar* addNode(RemoteVar* n) { nodeList.push_back(n); }
	Basic* addBasic(Basic* b) { basicList.push_back(b); }
	Pack* addPack(Pack* p) { packList.push_back(p); }
	Bits* addBits(Bits* b) { bitsList.push_back(b); }
	
	void error (const location& l, const std::string& m);
	void error (const std::string& m);
	
	void scan_begin();
	void scan_end();
	bool trace_scanning;
	
	std::string file;
	bool trace_parsing;
	
	// Getter functions for printers
	const std::vector<RemoteVar*>& getNodeList() const { return nodeList; }
	const std::vector<Basic*>& getBasicList() const { return basicList; }
	const std::vector<Pack*>& getPackList() const { return packList; }
	const std::vector<Bits*>& getBitsList() const { return bitsList; }
	
private:
	std::vector<RemoteVar*> nodeList;
	std::vector<Basic*> basicList;
	std::vector<Pack*> packList;
	std::vector<Bits*> bitsList;

};

}} // End of namespace okuz::parser

#endif
