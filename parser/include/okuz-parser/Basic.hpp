#ifndef PARSER_BASIC_HPP
#define PARSER_BASIC_HPP

#include <iostream>
#include "RemoteVar.hpp"

namespace okuz { namespace parser {

enum DataType {
	INT8,
	INT16,
	INT32,
	UINT8,
	UINT16,
	UINT32,
	FLOAT32
};

class Basic : public RemoteVar
{
public:
	Basic(std::string& n, DataType t);
	Basic(std::string& n, std::string& t);
	virtual ~Basic();
	
	void updateSize(int s);
	
	//Getters
	const std::vector<std::string> getMemberNames();
	int getSize() { return size; }
	DataType getDataType() { return type; }
	virtual std::string getDefinitionLine();
	virtual std::string getDefinitionLine2();
	virtual std::string getMemberInitLine();
	virtual std::string getPointerString();

protected:
	int size; // Number of elements (1 if not an array)
	DataType type;
	
private:
	DataType getTypeEnum(std::string& t);
	std::string getTypeString(DataType t);
	int rawSizeOf(DataType t);
};

}} // End of namespace okuz::parser

#endif
