#ifndef PARSER_BITS_HPP
#define PARSER_BITS_HPP

#include "RemoteVar.hpp"
#include <vector>

namespace okuz { namespace parser {

class Bits : public RemoteVar
{
public:
	Bits(std::string& n);
	virtual ~Bits();
	
	void addBit(std::string& n);
	
	// Getters
	const std::vector<std::string> getBitList() const { return bitList; }
	const std::vector<std::string> getMemberNames() { return bitList; }
	virtual std::string getDefinitionLine();
	virtual std::string getDefinitionLine2();
	virtual std::string getMemberInitLine();
	
private:
	std::vector<std::string> bitList;
};

}} // End of namespace okuz::parser

#endif
