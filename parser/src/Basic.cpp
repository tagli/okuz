#include "okuz-parser/Basic.hpp"

namespace okuz { namespace parser {

Basic::Basic(std::string& n, DataType t)
: RemoteVar(n), type(t), size(1)
{
	rawSize = rawSizeOf(t);
	rType = BASIC;
}

Basic::Basic(std::string& n, std::string& t)
: RemoteVar(n), size(1)
{
	type = getTypeEnum(t);
	rawSize = rawSizeOf(type);
}

Basic::~Basic()
{
}

void Basic::updateSize(int s)
{
	size = s;
	rawSize = rawSizeOf(type) * size;
}

std::string Basic::getDefinitionLine()
{
	std::ostringstream ss;
	if (volat)
		ss << "volatile ";
	ss << getTypeString(type) << " " << name << ";";
	return ss.str();
}

std::string Basic::getDefinitionLine2()
{
	std::ostringstream ss;
	if (size == 1)
		ss << "Single";
	else
		ss << "Multi";
	ss << "<" << getTypeString(type) << "> " << name << ";";
	return ss.str();
}

std::string Basic::getMemberInitLine()
{
	std::ostringstream ss;
	ss << name << "(";
	if (size > 1)
		ss << size << ", ";
	ss << "channel, ";
	ss << readCode << ", ";
	ss << writeCode << ", ";
	ss << saveCode << ", ";
	ss << loadCode << ", address),";
	return ss.str();
}

std::string Basic::getPointerString()
{
	std::ostringstream ss;
	ss << "(uint8_t*)(";
	if (size == 1) ss << "&";
	ss << name << ")";
	return ss.str();
}

// Private Functions
DataType Basic::getTypeEnum(std::string& t)
{
	if (t.compare("int8") == 0) return INT8;
	else if (t.compare("int16") == 0) return INT16;
	else if (t.compare("int32") == 0) return INT32;
	else if (t.compare("uint8") == 0) return UINT8;
	else if (t.compare("uint16") == 0) return UINT16;
	else if (t.compare("uint32") == 0) return UINT32;
	else return FLOAT32; // t.compare("flaot32") == 0
}

std::string Basic::getTypeString(DataType t)
{
	switch (t) {
	case INT8:
		return "int8_t";
	case UINT8:
		return "uint8_t";
	case INT16:
		return "int16_t";
	case UINT16:
		return "uint16_t";
	case INT32:
		return "int32_t";
	case UINT32:
		return "uint32_t";
	case FLOAT32:
		return "float";
	}
}

int Basic::rawSizeOf(DataType t)
{
	switch (t) {
	case INT8:
	case UINT8:
		return 1;
	case INT16:
	case UINT16:
		return 2;
	case INT32:
	case UINT32:
	case FLOAT32:
		return 4;
	}
}

const std::vector<std::string> Basic::getMemberNames()
{
	std::vector<std::string> dummy;
	return dummy;
}

}} // End of namespace okuz::parser
