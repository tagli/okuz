#include <iostream>
#include "okuz-parser/Bits.hpp"

namespace okuz { namespace parser {

Bits::Bits(std::string& n)
: RemoteVar(n)
{
	rawSize = 1;
	rType = BITS;
}

Bits::~Bits()
{
}

void Bits::addBit(std::string& n)
{
	bitList.push_back(n); 
	int nBits = bitList.size();
	if (nBits > 8) rawSize = 2;
	else if (nBits > 16) std::cout << "Too many bits! :(\n";
}

std::string Bits::getDefinitionLine()
{
	std::ostringstream ss;
	if (volat)
		ss << "volatile ";
	ss << "union _" << name << " " << name << ";";
	return ss.str();
}

std::string Bits::getDefinitionLine2()
{
	std::ostringstream ss;
	ss << "_" << name << " " << name << ";";
	return ss.str();
}

std::string Bits::getMemberInitLine()
{
	std::ostringstream ss;
	ss << name << "(channel, address),";
	return ss.str();
}

}} // End of namespace okuz::parser
