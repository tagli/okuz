#include "okuz-parser/RemoteVar.hpp"

namespace okuz { namespace parser {

RemoteVar::RemoteVar(std::string& n)
: name(n), readable(false), writable(false), eepromable(false),
volat(false), readCode(0), writeCode(0), saveCode(0), loadCode(0),
eepromOffset(0)
{
}

RemoteVar::~RemoteVar()
{
}

void RemoteVar::addFlag(char f)
{
	switch (f) {
	case 'r':
		readable = true;
		break;
	case 'w':
		writable = true;
		break;
	case 'e':
		eepromable = true;
		break;
	case 'v':
		volat = true;
		break;
	}
}

int RemoteVar::assignAccessCodes(int startId, int idInc)
{
	int id = startId;
	if (readable) {
		readCode = id;
		id += idInc;
	}
	if (writable) {
		writeCode = id;
		id += idInc;
	}
	if (eepromable) {
		saveCode = id;
		loadCode = id + idInc;
		id += (idInc * 2);
	}
	return id;
}

uint32_t RemoteVar::assignEepromOffset(uint32_t adr)
{
	if (eepromable) {
		eepromOffset = adr;
		if (rawSize % 2 == 0) return (adr + rawSize);
		else return (adr + rawSize + 1);
	}
	else {
		return adr;
	}
}

std::string RemoteVar::getPointerString()
{
	std::ostringstream ss;
	ss << "(uint8_t*)(&" << name << ")";
	return ss.str();
}

}} // End of namespace okuz::parser
