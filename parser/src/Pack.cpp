#include "okuz-parser/Pack.hpp"

namespace okuz { namespace parser {

Pack::Pack(std::string& n)
: RemoteVar(n), memberNamesReady(false)
{
	rawSize = 0;
	rType = PACK;
}

Pack::~Pack()
{
}

void Pack::addMember(Basic* m)
{
	memberList.push_back(m);
	rawSize += m->getRawSize();
}

std::string Pack::getDefinitionLine()
{
	std::ostringstream ss;
	if (volat)
		ss << "volatile ";
	ss << "struct _" << name << " " << name << ";";
	return ss.str();
}

std::string Pack::getDefinitionLine2()
{
	std::ostringstream ss;
	ss << "_" << name << " " << name << ";";
	return ss.str();
}

std::string Pack::getMemberInitLine()
{
	std::ostringstream ss;
	ss << name << "(channel, address),";
	return ss.str();
}

const std::vector<std::string> Pack::getMemberNames()
{
	std::vector<Basic*>::iterator it;
	
	if (!memberNamesReady) {
		for (it = memberList.begin(); it != memberList.end(); ++it) {
			std::string name = (*it)->getName();
			memberNames.push_back(name);
		}
		memberNamesReady = true;
	}
	
	return memberNames;
}

}} // End of namespace okuz::parser
