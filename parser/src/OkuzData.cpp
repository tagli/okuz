#include "okuz-parser/OkuzData.hpp"
#include "OkuzParser.hpp"

namespace okuz { namespace parser {

OkuzData::OkuzData()
: trace_scanning(false), trace_parsing(false)
{
}

OkuzData::~OkuzData()
{
}

int OkuzData::parse(const std::string& fileName)
{
	file = fileName;
	scan_begin();
	OkuzParser p(*this);
	p.set_debug_level(trace_parsing);
	int res = p.parse();
	scan_end();
	
	// Assigning message IDs and EEPROM offsets
	int msgId = 5;
	uint32_t offset = 0;
	std::vector<RemoteVar*>::iterator it;
	for (it = nodeList.begin(); it != nodeList.end(); ++it) {
		msgId = (*it)->assignAccessCodes(msgId, 5);
		offset = (*it)->assignEepromOffset(offset);
	}
	
	return res;
}

void OkuzData::error (const location& l, const std::string& m)
{
  std::cerr << l << ": " << m << std::endl;
}

void OkuzData::error (const std::string& m)
{
  std::cerr << m << std::endl;
}

}} // End of namespace okuz::parser
