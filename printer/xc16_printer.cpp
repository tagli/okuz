#include "xc16_printer.hpp"
#include <boost/algorithm/string.hpp>

xc16_printer::xc16_printer(OkuzData& o)
: od(o)
{
	data_h.open("data.h");
	data_c.open("data.c");
	msg_c.open("msg.c");
	offset_h.open("offset.h");
}

xc16_printer::~xc16_printer()
{
}

void xc16_printer::printOutput()
{
	printDataH();
	printDataC();
	printMsgC();
	printOffsetH();
}

// File printer main functions

void xc16_printer::printDataH()
{
	
	data_h << "#ifndef DATA_H\n#define DATA_H\n\n";
	data_h << "#include <xc.h>\n#include <stdint.h>\n\n";
	
	// Printing Pack Definitions
	
	vector<Pack*> packList = od.getPackList();
	vector<Pack*>::iterator p_it;
	data_h << "/* Pack Definitions */\n\n";
	for (p_it = packList.begin(); p_it != packList.end(); ++p_it) {
		printPackDefinition(*p_it);
		data_h << endl;
	}
	
	// Printing Bits Definitions
	
	vector<Bits*> bitsList = od.getBitsList();
	vector<Bits*>::iterator b_it;
	data_h << "/* Bits Definitions */\n\n";
	for (b_it = bitsList.begin(); b_it != bitsList.end(); ++b_it) {
		printBitsDefinition(*b_it);
		data_h << endl;
	}
	
	// Printing extern lines
	
	vector<RemoteVar*> nodeList = od.getNodeList();
	vector<RemoteVar*>::iterator n_it;
	data_h << "/* extern Declerations */\n\n";
	for (n_it = nodeList.begin(); n_it != nodeList.end(); ++n_it) {
		printExternLine(*n_it);
	}
	
	data_h << "\n#endif /* DATA_H */\n";
	data_h.close();
}

void xc16_printer::printDataC()
{
	data_c << "#include \"data.h\"\n\n";
	
	vector<RemoteVar*> nodeList = od.getNodeList();
	vector<RemoteVar*>::iterator it;
	for (it = nodeList.begin(); it != nodeList.end(); ++it) {
		data_c << (*it)->getDefinitionLine() << endl;
	}
	
	data_c << endl;
	data_c.close();
}

void xc16_printer::printMsgC()
{
	// Printing include directives
	
	msg_c << "#include <xc.h>\n";
	msg_c << "#include <uart.h>\n\n";
	msg_c << "#include \"buffer.h\"\n";
	msg_c << "#include \"data.h\"\n";
	msg_c << "#include \"offset.h\"\n";
	msg_c << "#include \"eeprom.h\"\n";
	msg_c << "#include \"usart.h\"\n\n";
	
	// Printing message definitions
	
	vector<RemoteVar*> nodeList = od.getNodeList();
	vector<RemoteVar*>::iterator it;
	for (it = nodeList.begin(); it != nodeList.end(); ++it) {
		string upper = boost::to_upper_copy((*it)->getName());
		int readCode = (*it)->getReadCode();
		int writeCode = (*it)->getWriteCode();
		int saveCode = (*it)->getSaveCode();
		int loadCode = (*it)->getLoadCode();
		if (readCode != 0)
			msg_c << "#define GET_" << upper << "_MSG " << readCode << endl;
		if (writeCode != 0)
			msg_c << "#define SET_" << upper << "_MSG " << writeCode << endl;
		if (saveCode != 0)
			msg_c << "#define SAVE_" << upper << "_MSG " << saveCode << endl;
		if (loadCode != 0)
			msg_c << "#define LOAD_" << upper << "_MSG " << loadCode << endl;
	}
	msg_c << endl;
	
	// Printing message cases
	msg_c << "void processMessages(buffer_t* b) {\n";
	msg_c << "\twhile (b->nData >= 2) {\n";
	msg_c << "\t\tint msg;\n\t\tDisableIntU1RX;\n";
	msg_c << "\t\tbufferDequeue(b, (uint8_t*)&msg, 2);\n";
	msg_c << "\t\tswitch (msg) {\n";
	for (it = nodeList.begin(); it != nodeList.end(); ++it) {
		string upper = boost::to_upper_copy((*it)->getName());
		int readCode = (*it)->getReadCode();
		int writeCode = (*it)->getWriteCode();
		int saveCode = (*it)->getSaveCode();
		int loadCode = (*it)->getLoadCode();
		if (readCode != 0) {
			msg_c << "\t\tcase GET_" << upper << "_MSG:\n";
			msg_c << "\t\t\tproTransmit(-GET_" << upper << "_MSG, ";
			msg_c << (*it)->getPointerString() << ", ";
			msg_c << (*it)->getRawSize() << ");\n\t\t\tbreak;\n";
		}
		if (writeCode != 0) {
			msg_c << "\t\tcase SET_" << upper << "_MSG:\n";
			msg_c << "\t\t\tbufferDequeue(b, ";
			msg_c << (*it)->getPointerString() << ", ";
			msg_c << (*it)->getRawSize() << ");\n\t\t\tbreak;\n";
		}
		if (saveCode != 0) {
			msg_c << "\t\tcase SAVE_" << upper << "_MSG:\n";
			msg_c << "\t\t\teepromWrite((uint16_t)&";
			msg_c << (*it)->getName() << ", ";
			msg_c << (*it)->getRawSize() << ", EEPROM_START_ADR + ";
			msg_c << upper << "_EEPROM_OFFSET);\n\t\t\tbreak;\n";
		}
		if (loadCode != 0) {
			msg_c << "\t\tcase LOAD_" << upper << "_MSG:\n";
			msg_c << "\t\t\teepromRead((uint16_t)&";
			msg_c << (*it)->getName() << ", ";
			msg_c << (*it)->getRawSize() << ", EEPROM_START_ADR + ";
			msg_c << upper << "_EEPROM_OFFSET);\n\t\t\tbreak;\n";
		}
	}
	msg_c << "\t\t}\n\t\tEnableIntU1RX;\n\t}\n}\n\n";
	
	msg_c.close();
}

void xc16_printer::printOffsetH()
{
	offset_h << "#ifndef OFFSET_H\n#define OFFSET_H\n\n";
	vector<RemoteVar*> nodeList = od.getNodeList();
	vector<RemoteVar*>::iterator it;
	for (it = nodeList.begin(); it != nodeList.end(); ++it) {
		string upper = boost::to_upper_copy((*it)->getName());
		if ((*it)->getLoadCode() != 0) {
			offset_h << "#define " << upper << "_EEPROM_OFFSET "; 
			offset_h << (*it)->getEepromOffset() << endl;
		}
	}
	offset_h << endl;
	offset_h << "#endif /* OFFSET_H */\n";
	offset_h.close();
}

// Helper Functions

void xc16_printer::printPackDefinition(Pack* p)
{
	vector<Basic*> memberList = p->getMemberList();
	vector<Basic*>::iterator it;
	
	data_h << "__pack struct _" << p->getName() << " {\n";
	
	for (it = memberList.begin(); it != memberList.end(); ++it) {
		data_h << "\t" << (*it)->getDefinitionLine() << endl;
	}
	
	data_h << "};\n";
	data_h << "typedef struct _" << p->getName() << " _" << p->getName() << ";\n";
}

void xc16_printer::printBitsDefinition(Bits* b)
{
	vector<string> bitList = b->getBitList();
	vector<string>::iterator it; 
	
	data_h << "__pack union _" << b->getName() << " {\n";
	
	int rawSize = b->getRawSize();
	if (rawSize == 1)
		data_h << "\tuint8_t all;\n";
	else if (rawSize == 2)
		data_h << "\tuint16_t all;\n";
	
	data_h << "\tstruct {\n";
	
	for (it = bitList.begin(); it != bitList.end(); ++it) {
		data_h << "\t\tuint8_t " << (*it) << " : 1;\n";
	}
	
	data_h << "\t};\n};\n";
	data_h << "typedef union _" << b->getName() << " _" << b->getName() << ";\n";
}

void xc16_printer::printExternLine(RemoteVar* r)
{
	data_h << "extern ";
	data_h << r->getDefinitionLine() << endl;
}
