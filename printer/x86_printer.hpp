#ifndef X86_PRINTER_HPP
#define X86_PRINTER_HPP

#include <iostream>
#include <fstream>
#include <vector>
#include <libgen.h>
#include "okuz-parser/OkuzData.hpp"

using namespace std;
using namespace okuz::parser;

class x86_printer
{
public:
	x86_printer(OkuzData& o, char* inFile);
	virtual ~x86_printer();
	
	void printOutput();
	
private:
	ofstream genBits_hpp, genBits_cpp, genPack_hpp, genPack_cpp;
	ofstream project_hpp, project_cpp;
	OkuzData& od;
	string projectName;
	
	// File printer main functions
	void printGenBitsHpp();
	void printGenBitsCpp();
	void printGenPackHpp();
	void printGenPackCpp();
	void printProjectHpp(string& pn);
	void printProjectCpp(string& pn);
	
	// Helper Functions
	void printBitsDec(Bits* b);
	void printBitsConst(Bits* b);
	void printPackDec(Pack* p);
	void printPackConst(Pack* p);
	
};

#endif
