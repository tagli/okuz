#include <iostream>
#include "okuz-parser/OkuzData.hpp"
#include "xc16_printer.hpp"
#include "x86_printer.hpp"

using namespace okuz::parser;

int main(int argc, char *argv[])
{
	OkuzData od;
	od.parse(argv[1]);
	
	xc16_printer slave(od);
	x86_printer master(od, argv[1]);
	slave.printOutput();
	master.printOutput();
}
