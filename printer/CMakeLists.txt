cmake_minimum_required(VERSION 2.8)
project(printer)

file(GLOB SOURCES "*.cpp")

add_executable(okuz-printer
	${SOURCES}
)

target_link_libraries(okuz-printer okuzparser)

install(
	TARGETS okuz-printer
	RUNTIME DESTINATION bin
)

