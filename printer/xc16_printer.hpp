#ifndef XC16_PRINTER_HPP
#define XC16_PRINTER_HPP

#include <iostream>
#include <fstream>
#include <vector>
#include "okuz-parser/OkuzData.hpp"

using namespace std;
using namespace okuz::parser;

class xc16_printer
{
public:
	xc16_printer(OkuzData& o);
	virtual ~xc16_printer();
	
	void printOutput();
	
private:
	ofstream msg_c, data_h, data_c, offset_h;
	OkuzData& od;
	
	// File printer main functions
	void printDataH();
	void printDataC();
	void printMsgC();
	void printOffsetH();
	
	// printDataH Helper Functions
	void printPackDefinition(Pack* p);
	void printBitsDefinition(Bits* b);
	void printExternLine(RemoteVar* r);
	
};

#endif
