#include "x86_printer.hpp"
#include <boost/algorithm/string.hpp>

x86_printer::x86_printer(OkuzData& o, char* inFile)
: od(o)
{
	string filePathless(basename(inFile));
	int dotPos = filePathless.find_last_of(".");
	projectName = filePathless.substr(0, dotPos);
	
	genBits_hpp.open("GenBits.hpp");
	genBits_cpp.open("GenBits.cpp");
	genPack_hpp.open("GenPack.hpp");
	genPack_cpp.open("GenPack.cpp");
	project_hpp.open((projectName + ".hpp").c_str());
	project_cpp.open((projectName + ".cpp").c_str());
}

x86_printer::~x86_printer()
{
}

void x86_printer::printOutput()
{
	printGenBitsHpp();
	printGenBitsCpp();
	printGenPackHpp();
	printGenPackCpp();
	printProjectHpp(projectName);
	printProjectCpp(projectName);
}

// File printer main functions

void x86_printer::printGenBitsHpp()
{
	genBits_hpp << "#ifndef GENBITS_HPP\n#define GENBITS_HPP\n\n";
	genBits_hpp << "#include <Bits.hpp>\n\n";
	genBits_hpp << "using namespace okuz::master;\n\n";
	
	vector<Bits*> bitsList = od.getBitsList();
	vector<Bits*>::iterator it;
	for (it = bitsList.begin(); it != bitsList.end(); ++it) {
		printBitsDec(*it);
	}
	
	genBits_hpp << "#endif // GENBITS_HPP\n";
	genBits_hpp.close();
}

void x86_printer::printGenBitsCpp()
{
	genBits_cpp << "#include \"GenBits.hpp\"\n\n";
	
	vector<Bits*> bitsList = od.getBitsList();
	vector<Bits*>::iterator it;
	for (it = bitsList.begin(); it != bitsList.end(); ++it) {
		printBitsConst(*it);
	}
	
	genBits_cpp.close();
}

void x86_printer::printGenPackHpp()
{
	genPack_hpp << "#ifndef GENPACK_HPP\n#define GENPACK_HPP\n\n";
	genPack_hpp << "#include <Pack.hpp>\n\n";
	genPack_hpp << "using namespace okuz::master;\n\n";
	
	vector<Pack*> packList = od.getPackList();
	vector<Pack*>::iterator it;
	for (it = packList.begin(); it != packList.end(); ++it) {
		printPackDec(*it);
	}
	
	genPack_hpp << "#endif // GENPACK_HPP\n\n";
	genPack_hpp.close();
}

void x86_printer::printGenPackCpp()
{
	genPack_cpp << "#include \"GenPack.hpp\"\n\n";
	
	vector<Pack*> packList = od.getPackList();
	vector<Pack*>::iterator it;
	for (it = packList.begin(); it != packList.end(); ++it) {
		printPackConst(*it);
	}
	
	genPack_cpp.close();
}

void x86_printer::printProjectHpp(string& pn)
{
	string upper = boost::to_upper_copy(pn);
	project_hpp << "#ifndef "<< upper << "_HPP\n";
	project_hpp << "#define " << upper << "_HPP\n\n";
	project_hpp << "#include <stdint.h>\n";
	project_hpp << "#include <Single.hpp>\n";
	project_hpp << "#include <Multi.hpp>\n";
	project_hpp << "#include <OkuzMaster.hpp>\n";
	project_hpp << "#include \"GenBits.hpp\"\n";
	project_hpp << "#include \"GenPack.hpp\"\n\n";
	
	project_hpp << "using namespace okuz::master;\n\n";
	
	project_hpp << "class " << pn << "\n{\nprivate:\n";
	project_hpp << "\tOkuzMaster& channel;\n\tuint8_t address;\n\npublic:\n";
	project_hpp << "\t" << pn << "(OkuzMaster& om, uint8_t adr);\n";
	project_hpp << "\tvirtual ~" << pn << "();\n\n";
	
	vector<RemoteVar*> nodeList = od.getNodeList();
	vector<RemoteVar*>::iterator it;
	for (it = nodeList.begin(); it != nodeList.end(); ++it) {
		project_hpp << "\t" << (*it)->getDefinitionLine2() << endl;
	}
	
	project_hpp << "};\n\n";
	project_hpp << "#endif // " << upper << "_HPP\n\n";
	project_hpp.close();
}

void x86_printer::printProjectCpp(string& pn)
{
	project_cpp << "#include \"" << pn << ".hpp\"\n\n";
	project_cpp << pn << "::" << pn << "(OkuzMaster& om, uint8_t adr)\n: ";
	
	vector<RemoteVar*> nodeList = od.getNodeList();
	vector<RemoteVar*>::iterator it;
	for (it = nodeList.begin(); it != nodeList.end(); ++it) {
		project_cpp << (*it)->getMemberInitLine() << endl;
	}
	
	project_cpp << "channel(om), address(adr)\n{\n}\n\n";
	project_cpp << pn << "::~" << pn << "()\n{\n}\n";
	project_cpp.close();
}

// Helper Funcitons

void x86_printer::printBitsDec(Bits* b)
{
	vector<string> bitList = b->getBitList();
	vector<string>::iterator it;
	string typeStr;
	if (bitList.size() <= 8)
		typeStr = "uint8_t";
	else if (bitList.size() <= 16)
		typeStr = "uint16_t";
		
	genBits_hpp << "class _" << b->getName() << " : public Bits<";
	genBits_hpp << typeStr << ">\n{\npublic:\n";
	genBits_hpp << "\t_" << b->getName() << "(OkuzMaster& om, uint8_t adr);\n\n";
	
	for (it = bitList.begin(); it != bitList.end(); ++it) {
		genBits_hpp << "\t_bitsHelper<" << typeStr << "> ";
		genBits_hpp << (*it) << ";\n";
	}
	
	genBits_hpp << "};\n\n";
}

void x86_printer::printBitsConst(Bits* b)
{
	vector<string> bitList = b->getBitList();
	vector<string>::iterator it;
	string typeStr;
	if (bitList.size() <= 8)
		typeStr = "uint8_t";
	else if (bitList.size() <= 16)
		typeStr = "uint16_t";
	
	genBits_cpp << "_" << b->getName() << "::_" << b->getName();
	genBits_cpp << "(OkuzMaster& om, uint8_t adr)\n:\n";
	
	int i = 0;
	for (it = bitList.begin(); it != bitList.end(); ++it) {
		genBits_cpp << (*it) << "(this, " << i << "),\n";
		++i;
	}
	
	genBits_cpp << "Bits<" << typeStr << ">(om, ";
	genBits_cpp << b->getReadCode() << ", ";
	genBits_cpp << b->getWriteCode() << ", ";
	genBits_cpp << b->getSaveCode() << ", ";
	genBits_cpp << b->getLoadCode() << ", adr)\n{\n}\n\n";
	
}

void x86_printer::printPackDec(Pack* p)
{
	vector<Basic*> memberList = p->getMemberList();
	vector<Basic*>::iterator it;
		
	genPack_hpp << "class _" << p->getName() << " : public Pack\n{\npublic:\n";
	genPack_hpp << "\t_" << p->getName() << "(OkuzMaster& om, uint8_t adr);\n\n";
	
	for (it = memberList.begin(); it != memberList.end(); ++it) {
		genPack_hpp << "\t" << (*it)->getDefinitionLine2() << endl;
	}
	
	genPack_hpp << "};\n\n";
}

void x86_printer::printPackConst(Pack* p)
{
	vector<Basic*> memberList = p->getMemberList();
	vector<Basic*>::iterator it;
	
	genPack_cpp << "_" << p->getName() << "::_" << p->getName();
	genPack_cpp << "(OkuzMaster& om, uint8_t adr)\n:\n";
	
	for (it = memberList.begin(); it != memberList.end(); ++it) {
		genPack_cpp << (*it)->getName() << "(om, ";
		genPack_cpp << (*it)->getReadCode() << ", ";
		genPack_cpp << (*it)->getWriteCode() << ", ";
		genPack_cpp << (*it)->getSaveCode() << ", ";
		genPack_cpp << (*it)->getLoadCode() << ", ";
		genPack_cpp << "0, true),\n";
	}
	
	genPack_cpp << "Pack(om, ";
	genPack_cpp << p->getReadCode() << ", ";
	genPack_cpp << p->getWriteCode() << ", ";
	genPack_cpp << p->getSaveCode() << ", ";
	genPack_cpp << p->getLoadCode() << ", adr, ";
	genPack_cpp << memberList.size() << ")\n{\n\tmembers.reserve(nMember);\n";
	
	for (it = memberList.begin(); it != memberList.end(); ++it) {
		genPack_cpp << "\tmembers.push_back(&" << (*it)->getName() << ");\n";
	}
	
	genPack_cpp << "\tallocateBuffer();\n}\n\n";
}
